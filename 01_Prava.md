---
author: Giovanni Foletto
title: Appunti di Programmazione Avanzata
subtitle: Sept. 2021 - Nov. 2021
rights: see LICENSE
ibooks:
  version: 1.0
---





# Programmazione Avanzata

## Abstract

In questo corso svilupperemo il discorso sul C++ moderno, applicando le teorie di programmazione avanzata e ad oggetti che questo linguaggio dalla versione del 2011 (C++11) ha iniziato a implementare.

Il corso si basa principalmente sull’idea espressa dall’articolo di Bjarne Stroustrup, “Thriving in a Crowded and Changing World: C++ 2006–2020”, che si interessa di spiegare di come il C++ sia cambiato dalla versione C++99, passando dalla C++11 (che introduce tantissime funzionalità di un normale linguaggio moderno), arrivando poi al C++20 (non trattato in questo corso, però).

L’articolo è disponibile online, a questo [sito](https://www.stroustrup.com/hopl20main-p5-p-bfc9cd4--final.pdf).

Il materiale del corso non è presente se non per casi particolari. 

Il codice delle lezioni, poi spiegato su queste pagine è disponibile nelle sottocartelle `lessons` e si ricollega al titolo della sezione. Il codice delle esercitazioni è disponibile nella cartella `labs` e diviso per ogni esercitazione.



## Index



[TOC]

# Intro

La storia del C++ è molto lunga:

* il C++ nasce nel 1979 per avere dei costrutti ad oggetti sul linguaggio molto prestante del C

  Si introducono: classi, costruttori/distruttori, concetto di pubblico e privato, concetto di eredità tra classi. Oltre a questo si introducono i concetti di controllo del tipo, oltre che dei miglioramento sulla gestione delle librerie che adesso possono supportare *tasks* (concetto di *coroutines* e *simultions*) otre che i vettori e le macro.

* Questo linguaggio viene apprezzato anche non propriamente nel campo della ricerca, siccome permette una serie di funzioni completamente staccate dal singolo scopo di studio, ma permette la creazione di veri e propri programmi.  Nel 1985 viene effettuata la prima release commerciale.

* Nel 1985 si ottiene la prima stardardizzazione del linguaggio **ANSI** e **ISO** e questo passo è molto importante perchè vuol dire che una serie di utilizzatori si mettono d’accordo per avere un tipo di scrittura di codice simile in tutto il campo di sviluppo.

  Si introducono poi il concetto di classi astratte, ereditarietà multipla, eccezioni, templates e si introduce la libreria di `iostream`.

* Nel 1998 compare lo standard che rimarrà per i successivi 10 anni circa: C++98. 

  Vengono creati i namespace, i cast di tipi, il tipo di dato bool e il concetto di `dynamic_cast`. Viene introdotta anche la libreria standard (STL - standard template labrary).

* Nel 2011 si introducono una serie di miglioria introdotte nel corso del decennio in cui lo standard è rimasto fermo e che una serie di altri linguaggi di programmazione “più moderni” avevano già implementato.

  Si introduce il *memory model* (diretto utilizzo dei threads), operatore `auto`, concetto di *for-range*, *const-expres*, *lambdas*, *user-defines literals* e molte altre migliorie. 

  Si introducono le librerie per la gestione dei *threads and locks*, *future*, *unique_ptr*, *shared_ptr*, *array*, *time and clocks*, *random numers*, *unordered containers (hash tables)* e altro ancora.

* Si inizia un periodico aggiornamento del linguaggio con degli standard a livello trieannale. Quindi si inizia a creare una serie di altre versioni. Seguono quindi di versioni C++14, C++17 e C++20. Molte nuove introduzioni sono di poco conto, però nel 2020 si sono introdotte delle librerie e concetti importanti per le *coroutines* e altri *improvements* molto particolari e avanzati

Il rapido sviluppo che si ha avuto nell’ultimo ventennio di C++ è dovuto a vari fattori. Primo di tutto la comunità di sviluppatori chiedeva dei costrutti particolari che rendesso più scorrevole lo sviluppo. In secondo luogo si ottengono degli attori di peso molto importanti, che erano interessati alle caratteristiche del C++ per le sue applicazioni (ad esempio come Microsoft, Google, ...). Per tutte le funzioni introdotte dal linguaggio e per le moltissime aziende che ne fanno uso, questo linguaggio (ora più che mai) ha un continuo miglioramento e sviluppo.

I principi implementativi del C++ si basano su dei concetti fondamentali:

* mantenere le cose per il più possibile semplice
* diretto controllo dell’hardware per avere diretto controllo sulle performace
* far rimanere il runtime system al minimo, mantenendo massima compatibilità
* ottime prestazioni soprattutto su hardware molto performante.



# Il C++11: Object Oriented

Discuteremo qualche caratteristica dello sviluppo in C++ ad oggetti, attraverso del codice che realizza questo diagramma UML:

<img src="image/image-20211117101304733.png" alt="image-20211117101304733" style="zoom:50%;" />

Questo diagramma significa:

* classe A ha un attributo `int`

* classe B ha un attributo `string`

* il simbolo indica la **composizione** delle classi: l’istanza di `B` non può sopravvire alla distruzione dell’istanza di `A`.  L’oggetto `A` può avere o non avere la classe `B`, ma questa è direttamente collegata al fatto che `A` esista.

  Tipici esempi di composizione può essere l’automobile e il suo telaio: se si distrugge il telaio non si ottiene più una macchina. 



## Codice 1 (Lezione 1)

**File di header di B**

```c++
#ifndef CLASSE_B
#define CLASSE_B

#include <iostream>
using namespace std;
#include <string>
class B{
    string s;
public:
    B(string _s);
    string get_s();
};
#endif
```

**File di implementazione di B**

```c++
#include "B.h"
B::B(string _s){
    s = _s;
}

string B::get_s(){
    return s;
}
```



Con questo esempio si può introdurre un paio di cose: se si richiama nel codice il costruttore a zero parametri facendo `B b;`, si ottiene un errore. Questo avviene perché nel C++ se si dichiara un costruttore con qualche parametro, allora non esiste più il costruttore di default a zero parametri.

Il punto di questo concetto è che **esistono metodi definiti anche se non dichiarati**, e sono:

* costruttori senza parametri `A::A()`
* costruttore per copia `A::A(const A&)`
* operatore di assegnazione `A::operator=(const A&)`
* distruttore `A::~A()`
* `new` e `delete`

e sono cose che si ottengono quando si definisce una classe, anche se non vengono definite dal programmatore. (Queste funzioni saranno viste meglio nella prossima sezione).

Passiamo al file **A.h** che ha lo scopo di realizzare la composizione “facoltativa” della classe `B`:

**File di header**

```c++
#ifndef CLASSE_A
#define CLASSE_A

#include <iostream>
using namespace std;
#include "B.h"
class A{
    int i; // richiesto dalla classe A, nella specifica UML
    B* pb; // composizione tramite un puntatore
    
public:
    A();						// costruttore a zero parametri
    A(int _i, string _s);		// costruttore a parametri
    A(const A& _a);			   	// costruttore di copia
    A& operator=(const A& _a); 	// operatore di assegnazione
    ~A();					   	// distruttore
    int get_i();				// getters
    string get_s();				
    void set_s(string _s);		// setters
};
#endif
```

In questo codice:

* `B* pd` viene utilizzato come un puntatore, ovvero una variabile tipizzata a un indirizzo

* nella specifica UML non sono definiti metodi, e quindi possiamo aggiungere dei metodi noi:

  * costruttore a zero parametri/con parametri: viene fatto per evitare che il costruttore a zero parametri sia presente
  * costruttore di copia 
  * operatore di assegnazione: nel C++ è possibile **ridefinire gli operatori** (dei metodi che sono presenti nelle operazioni). (vedi sotto)

* in questo codice si ottengono dei valori passati per:

  * valore: si copia la variabile nello stack di memoria della funzione. Questo valore può essere modificato, ma poi (a meno che non si ritorni il valore) *non* viene modificato nella funzione di partenza
  * puntatore: in questo caso il parametro da passare alla funzione è un indirizzo, e quindi andrà trattato come una variabile puntatore all’interno della funzione (di fatto si dovrà usare l’operatore `->`, oppure dereferenziare su ogni variabile di cui ci interessa sapere del valore `*b`).
  * referenza: il parametro da passare alla funzione è sempre una variabile che contiene un indirizzo di memoria, però attraverso la dereferenziazione io posso lavorare sul contenuto della variabile senza operatore freccetta o continuando a dereferenziare.

* il costruttore di copia serve a creare una seconda istanza di una classe attraverso la copia dei suoi parametri.

* la riderfinizione del operatore `=`: nel C++ quando si esegue un operazione tipo `a = a +a2` viene interpretata come se si stesse chiamando: `a::operator=(operator+(a, a2));` o `a::operator=(a::operator+(a2)); `(primo: `operator+` a due parametri, secondo: `operator+` a singolo parametro. Questi operatore non può essere implementato in entrambi i modi e quindi si deve scegliere quale implementare). Questo significa che il compilatore interpreta i segni attraverso delle funzioni che definiscono gli operatori che permettono di eseguire queste operazioni.

  L’operatore `operator=` di fatto serve per eseguire la funzione richiamata con `A a3; a3=a;`.

Il punto di questo discorso è che tutti questi parametri sono sempre definiti nella classe, ma questi operatori di copia e assegnazione non funzionano per la composizione, è necessario implementare un operatore con la **copia profonda**. Vediamo il caso nel **file di implementazione**:

```c++
#include "A.h"

using namespace std;

A::A(){
	i=0;
    pb=NULL; // si inizializza il puntatore al nulla
    cout << "Costruttore a zero parametri" << endl;
};

A::A(int _i, string _s){
    i = _i;
    pb = new B(_s); // creazione di un indirizzo a memoria dinamica per l'oggetto B, questa operazione si appoggia al costruttore definito in B
    cout << "Costruttore a due parametri" << endl;
}

A::A(const A& _a){
    // bisogna implementare la COPIA PROFONDA
    if(_a.pb != NULL){ // se pb di A non è nullo, allora 
        pb = new B(*(_a.pb)); // bisogna creare un nuovo oggetto B
    } else pb=NULL;
    cout << "Costruttore di copia" << endl;
}

A& A::operator=(const A& _a){
    if(this->pb == NULL){ 	// l'oggetto chiamante NON HA B
        if( _a.pb != NULL) {
            pb = new B(*(_a.pb));
        }
    } else{					// l'oggetto chiamante HA B
        if(_a.pb != NULL){  // l'oggetto chiamato HA B
            (*pb) = *(_a.pb);
        } else {			// l'oggetto chiamato NON HA B
            delete pb;
            pb = NULL;
        }
    }
    cout << "operatore di assegnazione" << endl;
    return *this; // bisogna tornare il valore se no non può funzionare a=(b=c)
}

A::~A(){
    if(pb != NULL){
        delete pb;
    }
    cout << "Distruttore" << endl;
}

string A::get_s(){
    if(pb->s != NULL){
        return pb->get_s();
    } else{
        return ""; // scelta implementativa
    }
}

void A::set_s(string _s){
    if(pb == NULL){ // se la string di pb è piena, allora viene sovrascritta
        pb = new B(_s);
    } else{ 		// se la string di pb è vuota, allora viene creata
        *(pb) = B(_s);
    }
}
```

In questo esempio è da notare l’implementazione della copia profonda: se si copiasse direttamente la classe `A`, allora si otterrebbero due istanze che puntano allo stesso oggetto `B` che `A` contiene. Questo è un comportamento che noi vogliamo evitare, quindi bisogna ridefinire il concetto di costruttore per copia.

L’operatore di assegnazione `operator=` ha bisogno di scrivere su un oggetto esistente, i parametri di un altro oggetto già esistente. Questo impone la creazione di 4 casistiche: se l’oggetto chiamante ha oppure non ha `B`, o se l’oggetto chiamato ha o non ha `B`.  Si noti l’operatore `this` che in questo caso nel C++ non è obbligatorio, ma in questo modo si rende più comprensibile la funzione.

Il codice si conclude con il distruttore. In Java questa funzione non esiste mai, siccome la memoria viene gestita dal JRE. Questo oggetto deve essere distrutto dal system di C++. Questo operatore deve essere liberato con questo operatore solo se il puntatore non è nullo.

Nel `get_s()` si fa la scelta di tornare una stringa vuota se non è stata definita in `B`. Si può comunque di decidere di imporre che la stringa ci sia alla creazione di `B` oppure sollevare un eccezione in questo caso.

Infine per testare tutta l’implementazione si crea un file **main.cpp**.



## Codice 2 (Lezione 2)

### Costruttore di copia profonda

In questo casi si ottiene una copia degli oggetti, ma nel caso di un puntatore si crea un oggetto uguale a quello di partenza, ma fecente parte di un’altra istanza.

In caso ci siano puntatori presenti nelle classi in questione, si ottiene un comportamento non voluto se non si implementa la *deep-copy*.

### Costruttore di copia superficiale

In questo caso si ottiene una totale copia degli oggetti, ma in caso di puntatore a un oggetto si copierà solo il puntatore (l’oggetto sarà quindi condiviso tra le due classi perché sarà la stessa istanza, ovvero la variabile punterà alla stessa istanza).

Quindi questo tipo di copia è esattamente sufficiente per altri casi in cui gli attributi **non sono puntatori**. 

### Costruttori

Come detto precedentemente se si definisce il costruttore a un parametro si elimina la possibilità di avere un costruttore a zero parametri predefinito. Se si vuole un costruttore a zero parametri con altri tipi di costruttori presenti nella classe, allora va esplicitamente dichiarato.

I costruttori a zero parametri nel C++ svolgono anche la funzione di **convertitori impliciti di tipo**. Questa operazione funziona in questo modo:

* ho una classe `A` definita come: 

  ```c++
  class A{
      int i;
  public:
      A(int _i);
  };
  ```

* una funzione `void f(A)` 

* se questa funzione fosse chiamata con `f(3)`, allora il compilatore non darebbe errori perché trasformerebbe la chiamata in `f(A::A(3))`, o equivalentemente `f(A(3))`.

Questo significa che il costruttore a un parametro viene utilizzato al deduttore di tipi per fare un cast implicito. 

Questo tipo di conversione avviene anche con tutte le funzioni che richiedono la classe `A` e contiene un tipo di dato presente in un costruttore di tipo a unico parametro di `A`.

Questo tipo di operazione è molto utile siccome permette di avere conversioni implicite di codice, semplificando la vita del programmatore.

Questo utilizzo dei costruttori a parametro singolo comunque **può essere evitato**, utilizzando la keyword `explicit`.  La classe di prima diventa quindi:

```c++
class A{
    int i;
public:
    explicit A(int _i);
};
```

E se quindi si creasse di nuovo il caso della chiamata della funzione `void f(A)`, in questo modo `f(3)`, allora il compilatore darebbe **errore di compilazione** perché è stato violato il vincolo sui tipi.

Chiaramente questa keyword non vieta di fare una conversione esplicita, e quindi passare alla funzione `f` un costruttore a un parametro con il valore `3`, come in questo caso: `f(A(3))`.

### Ridefinizione degli operatori

Il linguaggio C++ permette (come abbiamo già visto) la ridefinizione degli operatori, ovvero si può fare l’**overload** della funzione dell’operatore, ma **non si può aggiungere** nessun nuovo operatore.

Gli operatori in C++ sono tutti quei simboli che appaiono nelle operazioni, e sono:

* `operator=`
* `operator+`
* `operator-`
* ....

Gli operatori sono ridefinibili come metodi della classe o come funzioni esterne, ma **non** si possono seguire entrambe le cose. Eccetto l’operatore di assegnamento, che può essere **solo** un metodo.

Quando si ridefiniscono come **metodo**, il primo parametro è l’istanza chiamante, gli altri (se servono e se esistono) sono necessari al funzionamento o al controllo della funzione.

Se si implementa attraverso **funzioni esterne** tutti gli operandi sono parametri della funzione.

Un esempio di queste funzioni può essere:

```c++
// equazione
a = a1 + a2;
// chiamata del compilatore se metodo della classe
A::operator=(a1.operator+(a2)); 
// chiamata del compilatore se funzione esterna
A::operator=(operator+(a1, a2));
```

Da notare che ci sono operatori che tornano il risultato per valore e altri che tornano il risultato per referenza. Molto spesso gli operatori che modificano l’oggetto chiamante ritornano delle references, in modo da permettere delle esecuzioni a catena.

Il C++ permette anche di fare delle logiche piuttosto strane con questi operatori, e di fatto si riescono a definire funzioni che operano con delle classi completamente diverse.

Un elenco esaustivo degli operatori disponibili in C++ si può trovare [qui](https://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B).

Un grandissimo utilizzo di questi operatori è spesso unito al concetto di programmazione generica. Ad esempio la struttura dati `set` della `STL`. Questo operatore ha necessità di riordinare gli elementi e quindi ha bisogno di avere l’operatore `operator<` definito.

L’utilizzo della ridefinizione degli operatori è molto utile quando si applica a delle operazioni che utilizzano questi concetti naturalmente. Un esempio di questo concetto è una libreria per i numeri complessi: le operazioni di somma, differenza e tutte le altre operazioni matematiche legate a questi tipi di dati sono naturalmente utilizzate e qundi è comodo aver definito degli operatori che funzionino correttamente. 

Inoltre l’utilizzo degli operatori in equazioni più lunghe diventa molto comodo, siccome svolgere la stessa operazione con delle funzioni introdurrebbe un notevole aumento del codice e degraderebbe la leggibilità.

 Una grandissima inserzione del C++20 è il **three-way comparison** operator (`a <=> b`). Chi ha spinto per l’inserimento di questo operatore dice che da questo sarebbe possibile ricavare tutti gli altri.  



### Implementazione con metodi

(Queste due implementazioni sono state fatte in un unico file nella cartella della lezione 2.)

**File di header**

```c++
class A{
    int i;
public:
    A(int _i);
    A operator+(const A&)
};
```

**File di implementazione di A**
```c++
A::A(int _i){
    i = _i;
}

A A::operator+(const A& _a){
    return A(i+_a.i);
}
```
**File main.cpp**

```c++
int main(){
    A a1(1), a2(2), a3(3);
    a1 = a2 + a3; // funziona
    a1 = 2 + a3;  // NON compila -> non funziona
    a1 = a2 + 3;  // funziona
}
```
In questo codice possiamo notare alcune cose particolari:
* nel primo caso, `a1 = 2 + a3` non compila e non funziona.
* nel secondo caso, `a1 = a2 + 3` compila e funziona.

Questo avviene perché abbiamo definito l'operatore come metodo, e quindi si aspetta, in caso di applicazione, come primo parametro la classe chiamante, in modo poi da poter fare una conversione implicita di tipo in questo modo: `a2.operator+(A(3))`. Nel primo caso invece non esistendo una funzione definita come `operator+(a1, a2)` non può funzionare, perchè non dispone di un modo univoco di convertire un intero per l'operazione richiesta. Inoltre per questi tipi predefiniti non è mai possibile ridefinire gli operatori.
Se cerchiamo quindi di ottenere una funzionalità come quella illustrata sopra, in modo che funzioni in tutti i casi possibili di applicazione, non è una buona idea quella di utilizzare i metodi per la ridefinizione degli operatori.

### Implementazione con funzioni

**File di header**

```c++
class A{
    int i;
public:
    A(int _i);
    friend A operator+(const A&, const A&);
};
```

**File di implementazione**

```c++
A::A(int _i){
    i = _i;
}

A operator+(const A& _a, const A& _b){
    return A(_a.i+_b.i);
}
```

**File main.cpp**

```c++
int main(){
    A a1(1), a2(2), a3(3);
    a1 = a2 + a3; // funziona
    a1 = 2 + a3;  // funziona con la conversione implicita di tipo 
    a1 = a2 + 3;  // funziona con la conversione implicita di tipo
}
```

Ma qui viene indicata una nuova *keyword* `friend`. Questa parola chiave permette l’accesso a contenuti privati della classe anche se la funzione è definita altrove, in questo modo si può accedere ai contenuti privati senza *setter* e *getter*. Usando queste funzioni per recuperare il contenuto in realtà questa keyword non è necessaria, ma rende più leggibile il codice e migliora la comprensione immediata soprattutto in funzioni di operatori piuttosto lunghe.

La keyword `friend` va usato attentamente siccome evade alcuni principi dell’incapsulamento delle classi, rendendo di fatto pubblici alcuni valori privati. Nel caso preso in considerazione questa keyword è necessario. Spesso l’uso eccessivo di `friend` è sintomo di scarsa progettazione dell’architettura software, siccome ci sono parti del codice che dovrebbero essere accessibili ma che non lo sono.



### Operatori di decremento e incremento

Questi operatori servono per fare le operazioni di `++a`, `a++` oppure `--a`, `a--`. Come si vede anche dall’esempio poi si ha una valutazione:

* **postfissa** se si modifica la variabile e poi la si restituisce
* **prefissa** se si restituisce la variabile e poi la si modifica

Per usare queste due versioni si deve stare attenti a quando viene restituito il valore, soprattutto quando poi questo viene inserito in un equazione più complicata e con più di un passaggio.

Siccome il nome dei due operatori sarebbe uguale, nella dichiarazione postfissa si inserisce nella dichiarazione della funzione un valore `int` “dummy”, ovvero inutile. (molto poco elegante, ma di certo molto pratica). Questo intero non ha nessun valore e di fatto non viene passato un intero, ma serve al compilatore per distinguere le due funzioni.

Questi metodi quindi sono:

* `A& operator++();` **prefisso**
* `A operator++(int);` **postfisso**

Le implementazioni possono essere:

```c++
class A{
    int i;
public:
    A& opeator++(); 	// prefisso
    A operator++(int);	// postfisso
};

A& A::operator++(){
    ++i;
    return *this;
}

A A::operator++(int){
    A temp(*this);
    i++;
    return temp;
}
```

In questa funzione si ritorna il puntatore a questo oggetto, in modo da permettere le funzionalità in sequenza, tipo di svolgere l’operazione `++(++a)`.

Si restituisce `*this` nonostante ci sia una reference. Formalmente le reference sono valori, nonostante sia una variabile puntatore. Questo significa che se si ha una reference si deve deferenziare per ottenere il puntatore a cui l’oggetto indica, e non il suo valore. 

Di solito per il concetto che si ottiene prima o dopo il valore tornato rispetto alla modifica del suo valore, questi operatori vengono utilizzati solo singolarmente. Si noti inoltre che per soddisfare il requirements dell’operatore postfisso bisogna tornare il valore precedente e poi incrementare.



Esempio di operatori implementati per i numeri complessi in una libreria sono presenti nelle slides, oltre che nella cartella delle implementazione.

