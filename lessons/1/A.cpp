#include "A.h"

using namespace std;

A::A(){
	i=0;
    pb=NULL; // si inizializza il puntatore al nulla
    cout << "Costruttore a zero parametri" << endl;
};

A::A(int _i, string _s){
    i = _i;
    pb = new B(_s); // creazione di un indirizzo a memoria dinamica per l'oggetto B, questa operazione si appoggia al costruttore definito in B
    cout << "Costruttore a due parametri" << endl;
}

A::A(const A& _a){
    // bisogna implementare la COPIA PROFONDA
    this->i = _a.i;
    if(_a.pb != NULL){ // se pb di A non è nullo, allora 
        pb = new B(*(_a.pb)); // bisogna creare un nuovo oggetto B
    } else pb=NULL;
    cout << "Costruttore di copia" << endl;
}

A& A::operator=(const A& _a){
    this->i = _a.i;
    if(this->pb == NULL){ 	// l'oggetto chiamante NON HA B
        if( _a.pb != NULL) {
            pb = new B(*(_a.pb));
        }
    } else{					// l'oggetto chiamante HA B
        if(_a.pb != NULL){  // l'oggetto chiamato HA B
            (*pb) = *(_a.pb);
        } else {			// l'oggetto chiamato NON HA B
            delete pb;
            pb = NULL;
        }
    }
    cout << "operatore di assegnazione" << endl;
    return *this; // bisogna tornare il valore se no non può funzionare a=(b=c)
}

A::~A(){
    if(pb != NULL){
        delete pb;
    }
    cout << "Distruttore" << endl;
}

string A::get_s(){
    if(pb->get_s() != ""){
        return pb->get_s();
    } else{
        return ""; // scelta implementativa
    }
}
int A::get_i(){
    return i;
}

void A::set_s(string _s){
    if(pb == NULL){ // se la string di pb è piena, allora viene sovrascritta
        pb = new B(_s);
    } else{ 		// se la string di pb è vuota, allora viene creata
        *(pb) = B(_s);
    }
}