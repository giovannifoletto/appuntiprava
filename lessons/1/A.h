#ifndef CLASSE_A
#define CLASSE_A

#include <iostream>
using namespace std;
#include "B.h"
class A{
    int i; // richiesto dalla classe A, nella specifica UML
    B* pb; // composizione tramite un puntatore
    
public:
    A();						// costruttore a zero parametri
    A(int _i, string _s);		// costruttore a parametri
    A(const A& _a);			   	// costruttore di copia
    A& operator=(const A& _a); 	// operatore di assegnazione
    ~A();					   	// distruttore
    int get_i();				// getters
    string get_s();				
    void set_s(string _s);		// setters
};
#endif