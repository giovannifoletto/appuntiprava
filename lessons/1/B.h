#ifndef CLASSE_B
#define CLASSE_B

#include <iostream>
using namespace std;
#include <string>
class B{
    string s;
public:
    B(string _s);
    string get_s();
};
#endif