// Giovanni Foletto

#include <iostream>
#include "A.h"
#include "B.h"

using namespace std;

int main(){
    A a;
    cout << "a.i : " << a.get_i() << endl;
    A a1;
    cout << "a1.i : " << a1.get_i() << endl;
    a1 = a;
    cout << "a1.i : " << a1.get_i() << endl;
    A a2(a);
    cout << "a2.i : " << a2.get_i() << endl;
    A* pa = new A(a);
    cout << "pa.i : " << pa->get_i() << endl;
    delete pa;
    B p("prova");
    cout << "p.s : " << p.get_s() << endl;
    A a3(1, "prova2");
    cout << "a3.s : " << a3.get_s() << endl;
    cout << "a3 : " << a3.get_i() << " " << a3.get_s() << endl;
    a2.set_s("Cambiato");
    a3.set_s("Altro valore");
    cout << "a2.s : " << a2.get_s() << endl;
    cout << "a3.s : " << a3.get_s() << endl;
    a3 = a2;
    cout << "a3.s : " << a3.get_s() << endl;
    a2.set_s("al freddo");
    cout << endl;
    cout << "a2.s : " << a2.get_s() << endl;
    cout << "a3.s : " << a3.get_s() << endl;
    return 0;
}