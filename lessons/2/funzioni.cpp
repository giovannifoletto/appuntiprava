// Giovanni Foletto
// implementazione degli operatori con le delle funzioni 'friend' delle classi
// la classe ha implementato tutto assieme (sia header che funzioni) per comodità e brevità
//      un organizzazione del codice in questa maniera non è consigliata per l'esame.

#include <iostream>
using namespace std;

class A{
    int i;
public:
    A(int _i){
        i = _i;
    }
    friend A operator+(const A& _a, const A& _b){
        return A(_a.i+_b.i);
    }
    int get_i(){
        return i;
    }
};

int main(){
    A a1(1), a2(2), a3(3);
    cout << "a1: " << a1.get_i() << " , a2: " 
        << a2.get_i() << " , a3: " <<  a3.get_i() << endl;
    a1 = a2+a3; // Ok
    a1 = 5 + a3;
    cout << "a1: " << a1.get_i() << " , a2: " 
        << a2.get_i() << " , a3: " <<  a3.get_i() << endl;
    a1 = a2 + 8; // Ok
    cout << "a1: " << a1.get_i() << " , a2: " 
        << a2.get_i() << " , a3: " <<  a3.get_i() << endl;
    return 0;
}