// Giovanni Foletto
// Implementazione operatori di incremento post/pre-fissa.

#include <iostream>
using namespace std;

class A{
    int i;
public:
    A(int _i){
        i = _i;
    }
    A& operator++(); 	// prefisso
    A operator++(int);	// postfisso
    int get_i(){
        return i;
    }
};

A& A::operator++(){
    ++i;
    return *this;
}

A A::operator++(int){
    A temp(*this);
    i++;
    return temp;
}

int main(){
    A a1(1), a2(2);
    cout << "a1: " << a1.get_i() << " a2: " << a2.get_i() << endl;
    cout << "a1++: " << (a1++).get_i() << endl;
    cout << "++a2: " << (++a2).get_i() << endl;
    cout << "a1: " << a1.get_i() << " a2: " << a2.get_i() << endl;
    return 0;
}