#ifndef __NUMERI_COMPLESSI__
#define __NUMERI_COMPLESSI__

#include <iostream>

class Complex{
    int im;
    int re;
public:
    Complex();
    Complex(int _im, int _re);
    int get_im();
    int get_re();

    float abs();

    // operators
    friend Complex operator+(Complex& _c1, Complex& _c2);
    friend Complex operator-(Complex& _c1, Complex& _c2);
    friend Complex operator*(Complex& _c1, Complex& _c2);
    friend bool operator!=(Complex& _c1, Complex& _c2);
    friend std::ostream& operator<<(std::ostream& os, const Complex& c);
};


#endif