// Giovanni Foletto
// Testing Complex library

#include <iostream>
#include "Complex.h"
using namespace std;

int main(){
    Complex a(3, 4), a1(4, 5);
    cout << "a: " << a << "  |a|: " << a.abs() << endl;
    cout << "a1: " << a1 << "  |a1|: " << a1.abs() << endl;
    a = a+a1;
    cout << "Sum: " << a << endl;
    a = a-a1;
    cout << "Subt: " << a << endl;
    a = a*a1;
    cout << "Products: " << a << endl;
    cout << "isEqual: " << (a!=a1) << endl;
    
    return 0;
}