// implementazione Complex.h
#include "Complex.h"
#include <iostream>
#include <math.h>
using namespace std;

Complex::Complex(){
    im = 0;
    re = 0;
};

Complex::Complex(int _im, int _re){
    im = _im;
    re = _re;
};

int Complex::get_im(){
    return im;
};
int Complex::get_re(){
    return re;
};

Complex operator+(Complex& _c1, Complex& _c2){
    return Complex(_c1.im+_c2.im, _c1.re+_c2.re);
};

Complex operator-(Complex& _c1, Complex& _c2){
    return Complex(_c1.im-_c2.im, _c1.re-_c2.re);
};

Complex operator*(Complex& c1, Complex& c2){
    return Complex((c1.im*c2.im)-(c1.re*c2.re),  (c1.re*c2.im + c1.im*c2.re));
};


std::ostream& operator<<(std::ostream& os, const Complex& c){
    if(c.im >= 0 && c.re >= 0) os << c.im << " + " << c.re << "i";
    else if(c.im >= 0 && c.re < 0) os << c.im << " - " << c.re << "i";
    else if(c.im < 0 && c.re <0) os << c.im << " " << c.re << "i";
    else if(c.im < 0 && c.re > 0) os << c.im << " + " << c.re << "i";
    else os << c.im << " " << c.re << "i";
    return os;
};

bool operator!=(Complex& _c1, Complex& _c2){
    return (_c1.im != _c2.im && _c1.re != _c2.re);
};

float Complex::abs(){
    return sqrt(im*im+re*re);
};