# Numeri complessi
Implementazione di una libreria per la gestione dei numeri complessi.
In questo codice si verificherà il funzionamento di tutti gli operatori matematici tra due numeri complessi.

L'implementazione è fatta solo con dei valori `int`, siccome nel corso non sono ancora state presentate modalità di programmazione generica.
Tutti gli operatori verranno implementati come funzioni esterne e quindi con l'utilizzo della *keyword* `friend`.

Compila e runna con: `g++ Complex.cpp main.cpp -o complex.out && ./complex` [IN LINUX]